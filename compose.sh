#!/bin/bash
#   CSS
echo -e "\nCopying css to dist"
mkdir -p 'dist/' && cp -R html/css dist/css
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   JS
echo -e "\nCopying js to dist"
mkdir -p 'dist/' && cp -R html/js dist/js
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Revolution js
echo -e "\nCopying revolution js to dist"
mkdir -p 'dist/' && cp -R html/revolution/js dist/revolution_js
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Revolution css
echo -e "\nCopying revolution css to dist"
mkdir -p 'dist/' && cp -R html/revolution/js dist/revolution_css
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   fonts
echo -e "\nCopying fonts to dist"
mkdir -p 'dist/' && cp -R html/fonts dist/fonts
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   images
echo -e "\nCopying images to dist"
mkdir -p 'dist/' && cp -R src/images dist/images
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   videos
echo -e "\nCopying videos to dist"
mkdir -p 'dist/' && cp -R src/videos dist/videos
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Sitemap
echo -e "\nGenerating Sitemap"
node ./src/data/compose/sitemap.js
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Templates
echo -e "\nComposing templates"
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
hbs src/index.hbs --partial 'src/partials/*.hbs' --output dist/home/ && hbs src/aboutus/index.hbs --partial 'src/partials/*.hbs' --output dist/aboutus/ && hbs src/services/index.hbs --partial 'src/partials/*.hbs' --output dist/services/ && hbs src/portfolio/index.hbs --partial 'src/partials/*.hbs' --output dist/portfolio/ && hbs src/contactus/index.hbs --partial 'src/partials/*.hbs' --output dist/contactus/ &&  exit 1;