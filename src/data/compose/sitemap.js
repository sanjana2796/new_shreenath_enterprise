var _ = require('lodash');
var fs = require("fs");
var walk = require('walk');
var mkdirp = require("mkdirp");
const { SitemapStream, streamToPromise } = require('sitemap');
const smStream = new SitemapStream({ hostname: 'http://newshreenathenterprise.test' })

let files = [];
let excludes = ['404', 'css', 'js', 'img', '.DS_Store'];
var walker = walk.walk("dist", { followLinks: false });
walker.on('file', function (root, stat, next) {
    // console.log(root);
    // Do not include
    // Add this file to the list of files
    if (_.includes(stat.name, '.html')) {
        let tf = true;
        _.each(excludes, (ex) => {
            if (_.includes(root, ex)) {
                tf = false;
            }
        });
        if (tf) {
            files.push(root + '/' + stat.name);
        }
        else {
            // console.log('Rejecting', root);
        }
    }
    else {
        // console.log('Rejecting', stat.name);
    }
    next();
});
walker.on('end', function () {
    _.each(files, (file, j) => {
        // Formatting URL
        file = file.replace('dist', '').replace('index.html', '');
        smStream.write({
            url: file,
            changefreq: 'weekly',
            priority: 0.8
        })
    });
    smStream.end();
    streamToPromise(smStream)
        .then(sm => {
            fs.writeFileSync('dist/sitemap.xml',sm.toString());
            console.log('Sitemap composed to [dist/sitemap.xml]');
        })
        .catch(console.error);
});